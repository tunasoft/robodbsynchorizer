package com.setek.tr.roboDbSynchorizer.Synchorizer;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.setek.tr.roboDbSynchorizer.DbOperation.DbOperation;
import com.setek.tr.roboDbSynchorizer.model.Lief;

public class LiefSynchronized {
	
	private static final Logger log=Logger.getLogger(LiefSynchronized.class);
	
	public boolean synchorizedLiefData() throws IOException {
		log.info("LiefSynchronized.synchorizedLiefData()"); 
		List<Lief> liftsDwh = getLiefDataFromDwh();

		List<Lief> liftsRobo = getLiefDataFromRobo();

		List<Lief> saveList = new ArrayList<Lief>();

		for (Lief lief : liftsDwh) {

			if (!isExistLiefFromRobo(lief, liftsRobo)) {
				saveList.add(lief);
			}
		}
		boolean success = saveLiefToRobo(saveList);
		log.info("LiefSynchronized.synchorizedLiefData() done...");
		return success;
	

	}

	private boolean saveLiefToRobo(List<Lief> saveList) throws SecurityException, IOException {
		log.info("LiefSynchronized.saveLiefToRobo()");
		Connection conRobo = DbOperation.getRoboDbConnection();

		// the mysql insert statement
		String query = " insert into roboback.lief (`SATICI_NO`,\r\n" + "`SATICI_ADI`,\r\n" + "`TAX_NUMBER`,\r\n"
				+ "`ADRES`,\r\n" + "`SEHIR`,\r\n" + "`STATUS`,\r\n" + "`TAX_OFFICE`,\r\n" + "`ULKE_KODU`,\r\n"
				+ "`VC_FLAG`,\r\n" + "`POSTA_KODU`,\r\n" + "`TELEFON`,\r\n" + "`EMAIL`)"
				+ " values (?,?,?, ?, ?,?,?,?,?,?,?,?)";

		try {

			for (Lief lief : saveList) {
				PreparedStatement preparedStmt = conRobo.prepareStatement(query);
				preparedStmt.setString(1, lief.getSATICI_NO());
				preparedStmt.setString(2, lief.getSATICI_ADI());
				preparedStmt.setString(3, lief.getTAX_NUMBER());
				preparedStmt.setString(4, lief.getADRES());
				preparedStmt.setString(5, lief.getSEHIR());
				preparedStmt.setString(6, lief.getSTATUS());
				preparedStmt.setString(7, lief.getTAX_OFFICE());
				preparedStmt.setString(8, lief.getULKE_KODU());
				preparedStmt.setString(9, lief.getVC_FLAG());
				preparedStmt.setString(10, lief.getPOSTA_KODU());
				preparedStmt.setString(11, lief.getTELEFON());
				preparedStmt.setString(12, lief.getEMAIL());

				// execute the preparedstatement
				preparedStmt.execute();
			}
			conRobo.close();
			log.info("LiefSynchronized.saveLiefToRobo() is done");
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}

		log.info("LiefSynchronized.saveLiefToRobo() is done");
		return false;
	}

	private boolean isExistLiefFromRobo(Lief lief, List<Lief> liftsRobo) {
		for (Lief liefRobo : liftsRobo) {

			if (liefRobo.getSATICI_NO().equals(liefRobo.getSATICI_NO())) {
				return true;
				
			}
		}
		return false;
	}

	private List<Lief> getLiefDataFromRobo() throws SecurityException, IOException {
		log.info("LiefSynchronized.getLiefDataFromRobo()");
		String sql = "SELECT * FROM roboback.lief";

		Connection conDwh = DbOperation.getRoboDbConnection();

		ResultSet resultSet = null;
		try {
			resultSet = DbOperation.getQueryResultSet(conDwh, sql);

			List<Lief> liefList = new ArrayList<Lief>();

			while (resultSet.next()) {
				Lief lief = new Lief();

				lief.setSATICI_NO(resultSet.getString("SATICI_NO"));
				liefList.add(lief);
			}
			log.info("LiefSynchronized.getLiefDataFromRobo() is done");
			return liefList;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			// We have to close the connection and release the resources used.
			// Closing the statement results in closing the resultSet as well.
			try {
				resultSet.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}

			try {
				conDwh.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}
		log.info("LiefSynchronized.getLiefDataFromRobo() is done");
		return new ArrayList<Lief>();
	}

	public List<Lief> getLiefDataFromDwh() throws IOException {

		log.info("LiefSynchronized.getLiefDataFromDwh()");

		String sql = "SELECT * FROM(SELECT LIEF_NO SATICI_NO\r\n" + 
				"				 ,REPLACE (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LIEF_NAME,'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') , '''','') SATICI_ADI\r\n" + 
				"				 ,TAX_NO AS TAX_NUMBER\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TAX_OFFICE,'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') ,'''','') AS TAX_OFFICE\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST((SELECT MAX(STRASSE1)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO  )\r\n" + 
				"				 ||' '||\r\n" + 
				"				 (SELECT MAX(STRASSE2)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO ) AS VARCHAR ),'�','S'),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') ,'''','')AS ADRES\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((SELECT MAX(ORT)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO ),'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C'),'''','')  SEHIR\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((SELECT MAX(ISO_COUNTRY_CODE)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO ),'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C')  ULKE_KODU\r\n" + 
				"				 ,1 AS VC_FLAG\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((SELECT MAX(PLZ)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO ),'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') POSTA_KODU\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((SELECT MAX(TELEFON)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO ),'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') TELEFON\r\n" + 
				"				 ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((SELECT MAX(EMAIL)  FROM LIEF_ADR WHERE LIEF_ADR.LIEF_NO=LIEF.LIEF_NO GROUP BY LIEF_ADR.LIEF_NO ),'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') EMAIL\r\n" + 
				"				FROM LIEF )";

		Connection conDwh = DbOperation.getDwhConnection();

		ResultSet resultSet = null;
		try {
			resultSet = DbOperation.getQueryResultSet(conDwh, sql);

			List<Lief> liefList = new ArrayList<Lief>();

			while (resultSet.next()) {
				Lief lief = new Lief();

				lief.setSATICI_NO(resultSet.getString("SATICI_NO"));
				lief.setSATICI_ADI(resultSet.getString("SATICI_ADI"));
				lief.setTAX_NUMBER(resultSet.getString("TAX_NUMBER"));
				lief.setADRES(resultSet.getString("ADRES"));
				lief.setSEHIR(resultSet.getString("SEHIR"));
				lief.setTAX_OFFICE(resultSet.getString("TAX_OFFICE"));
				lief.setULKE_KODU(resultSet.getString("ULKE_KODU"));
				lief.setVC_FLAG(resultSet.getString("VC_FLAG"));
				lief.setPOSTA_KODU(resultSet.getString("POSTA_KODU"));
				lief.setTELEFON(resultSet.getString("TELEFON"));
				lief.setEMAIL(resultSet.getString("EMAIL"));
				liefList.add(lief);
			}

			log.info("LiefSynchronized.getLiefDataFromDwh() is done");

			return liefList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			// We have to close the connection and release the resources used.
			// Closing the statement results in closing the resultSet as well.
			try {
				resultSet.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}

			try {
				conDwh.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}

		log.info("LiefSynchronized.getLiefDataFromDwh() is done");

		return new ArrayList<Lief>();
	}

}
