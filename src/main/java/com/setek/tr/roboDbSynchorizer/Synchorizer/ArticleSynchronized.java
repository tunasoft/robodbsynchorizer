package com.setek.tr.roboDbSynchorizer.Synchorizer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.setek.tr.roboDbSynchorizer.DbOperation.DbOperation;
import com.setek.tr.roboDbSynchorizer.model.Article;

public class ArticleSynchronized {

	private static final Logger log=Logger.getLogger(ArticleSynchronized.class);
	

	public boolean synchorizedArticleData() {
        log.info("ArticleSynchronized.synchorizedArticleData()");
		List<Article> artsDwh = getArticleListFromDwh();
		
		List<Article> artsRobo = getArticleListFromRobo();
		
		List<Article> saveList = new ArrayList<Article>();
		
		for (Article article : artsDwh) {
			
			 if (!isExistArticleFromRobo(article,artsRobo)) {
				 saveList.add(article);
			 }			
		}
		boolean success = saveArticleToRobo(saveList);
		log.debug("ArticleSynchronized.synchorizedArticleData() done...");
		return success;

	}

	private static boolean saveArticleToRobo(List<Article> saveList) {
		log.debug("ArticleSynchronized.saveArticleToRobo()");
		Connection conRobo = DbOperation.getRoboDbConnection();
		
		  // the mysql insert statement
	      String query = " insert into roboback.article_media_saturn_robo (ART_NO, ART_NAME, ART_TYP, LOESCH_KZ, BRAND , EAN_NO,STATUS,UOM_PURCHASING,"
	      		+ "COLOR_CODE, LOCAL_PRODUCT_CODE,ARTICLE_CATEGORY)"
	        + " values (?,?,?,?,?,?,?,?,?,?,?)";
		
	      try {
	    	  
	    	  for (Article article : saveList) {
	    		  PreparedStatement preparedStmt  = conRobo.prepareStatement(query);
	    		  preparedStmt.setString (1,article.getART_NO());
	    	      preparedStmt.setString (2, article.getART_NAME());
	    	      preparedStmt.setString (3, article.getART_TYP());
	    	      preparedStmt.setString (4, article.getLOESCH_KZ());
	    	      preparedStmt.setString (5, article.getBRAND());
	    	      preparedStmt.setString (6, article.getEAN_NO());
	    	      preparedStmt.setString (7, article.getSTATUS());
	    	      preparedStmt.setString (8, article.getUOM_PURCHASING());
	    	      preparedStmt.setString (9, article.getCOLOR_CODE());
	    	      preparedStmt.setString (10, article.getLOCAL_PRODUCT_CODE());
	    	      preparedStmt.setString (11, article.getARTICLE_CATEGORY());
//	    	      
//	    	      
	    	   // execute the preparedstatement
	    	      preparedStmt.execute();
			}
	    	  conRobo.close();
	    	  log.info("ArticleSynchronized.saveArticleToRobo() is done");
	    	  return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	      log.info("ArticleSynchronized.saveArticleToRobo() is done");
	      return false;
	      
	}
	

	

	private static boolean isExistArticleFromRobo(Article article, List<Article> artsRobo) {
		for (Article artRobo : artsRobo) {
			
			if (artRobo.getART_NO().equals(article.getART_NO())) {
				return true;
		
			}
		}	
		return false;
	}

	public  List<Article> getArticleListFromDwh() {
		log.info("ArticleSynchronized.getArticleListFromDwh()");

		String sql = "SELECT A.ART_NO ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(A.ART_BEZ,'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') AS ART_NAME ,A.ART_TYP \r\n" + 
				"				,A.LOESCH_KZ \r\n" + 
				"				,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(HRS_TEXT,'�','S' ),'�','I'),'�','O'),'�','U'),'�','G'),'�','C') AS BRAND \r\n" + 
				"				, EA.EAN_NO \r\n" + 
				"				,'Adet' AS uom_purchasing \r\n" + 
				"				,'' AS color_code \r\n" + 
				"				,A.ART_NO AS local_product_code \r\n" + 
				"				,(SELECT  MAX(WGR_BEZ) FROM WGR WHERE WGR.WGR_NO=A.WGR_NO) AS article_category \r\n" + 
				"				FROM ARTIKEL A  \r\n" + 
				"				INNER JOIN ADMIN.EAN EA ON EA.ART_NO = A.ART_NO";

		Connection conDwh = DbOperation.getDwhConnection();
		ResultSet resultSet = null;
		try {
			resultSet = DbOperation.getQueryResultSet(conDwh, sql);

			List<Article> articleList = new ArrayList<Article>();

			while (resultSet.next()) {
				Article art = new Article();

				art.setART_NO(resultSet.getString("ART_NO"));
				art.setART_NAME(resultSet.getString("ART_NAME"));
				art.setART_TYP(resultSet.getString("ART_TYP"));
				art.setLOESCH_KZ(resultSet.getString("LOESCH_KZ"));
				art.setBRAND(resultSet.getString("BRAND"));
				art.setEAN_NO(resultSet.getString("EAN_NO"));
				art.setUOM_PURCHASING(resultSet.getString("UOM_PURCHASING"));
				art.setCOLOR_CODE(resultSet.getString("COLOR_CODE"));
				art.setLOCAL_PRODUCT_CODE(resultSet.getString("LOCAL_PRODUCT_CODE"));
				art.setARTICLE_CATEGORY(resultSet.getString("ARTICLE_CATEGORY"));
				
				articleList.add(art);
			}
			log.info("ArticleSynchronized.getArticleListFromDwh() is done");
			return articleList;
		} catch (SQLException e) {
			log.error(e.getMessage());
		} finally {
			// We have to close the connection and release the resources used.
			// Closing the statement results in closing the resultSet as well.
			try {
				resultSet.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}

			try {
				conDwh.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}
		log.info("ArticleSynchronized.getArticleListFromDwh() is done");
		return new ArrayList<Article>();

	}

	public static List<Article> getArticleListFromRobo() {
		log.info("ArticleSynchronized.getArticleListFromRobo()");
		String sql = "SELECT *  FROM roboback.article_media_saturn_robo";

		Connection conDwh = DbOperation.getRoboDbConnection();

		ResultSet resultSet = null;

		try {
			resultSet = DbOperation.getQueryResultSet(conDwh, sql);

			List<Article> articleList = new ArrayList<Article>();

			while (resultSet.next()) {
				Article art = new Article();

				art.setART_NO(resultSet.getString("ART_NO"));
//				art.setART_NAME(resultSet.getString("ART_NAME"));
//				art.setART_TYP(resultSet.getString("ART_TYP"));
//				art.setLOESCH_KZ(resultSet.getString("LOESCH_KZ"));
//				art.setBRAND(resultSet.getString("BRAND"));
//				art.setEAN_NO(resultSet.getString("EAN_NO"));
//				art.setUOM_PURCHASING(resultSet.getString("UOM_PURCHASING"));
				
				articleList.add(art);
			}
			log.info("ArticleSynchronized.getArticleListFromRobo() is done");
			return articleList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			// We have to close the connection and release the resources used.
			// Closing the statement results in closing the resultSet as well.
			try {
				resultSet.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}

			try {
				conDwh.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}
		log.info("ArticleSynchronized.getArticleListFromRobo() is done");
		return new ArrayList<Article>();

	}

	



	

}
