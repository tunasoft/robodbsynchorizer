package com.setek.tr.roboDbSynchorizer.Synchorizer;
import java.io.IOException;
import java.util.logging.FileHandler;
import org.apache.log4j.Logger;
import java.util.logging.SimpleFormatter;
public class Worker extends Thread {


	private static final Logger log=Logger.getLogger(Worker.class);
	@Override
	public void run()  {
		try {
			 
			log.info("Worker.run()");
		} catch (SecurityException e1) {
			log.error(e1.getMessage());
		}  

		LiefSynchronized liefSync = new LiefSynchronized();
		try {
			liefSync.synchorizedLiefData();
		} catch (IOException e) {
			log.error(e.toString());
		}
		
		try {
			ArticleSynchronized artSyn = new ArticleSynchronized(); 
			artSyn.synchorizedArticleData();
		} catch (Exception e) {
			log.error(e.toString());
		}
		
		

		try {
			TransferSynchronized transSync = new TransferSynchronized();
			transSync.synchorizedTransferData();
		} catch (SecurityException e) {
			log.error(e.toString());
		} catch (IOException e) {
			log.error(e.toString());
		}

		log.info("Worker.run() is done");
		
	}
	
	
}
