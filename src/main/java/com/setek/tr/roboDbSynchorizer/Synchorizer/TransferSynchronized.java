package com.setek.tr.roboDbSynchorizer.Synchorizer;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.setek.tr.roboDbSynchorizer.DbOperation.DbOperation;
import com.setek.tr.roboDbSynchorizer.model.Transfer;

public class TransferSynchronized {

	private static final Logger log=Logger.getLogger(TransferSynchronized.class);
	public boolean synchorizedTransferData() throws SecurityException, IOException {
		
		log.debug("TransferSynchronized.synchorizedTransferData()"); 
		List<Transfer> transfersDwh = getTransferDataFromDwh();

		List<Transfer> transfersRobo = getTransferDataFromRobo();

		List<Transfer> saveList = new ArrayList<Transfer>();

		for (Transfer transfer : transfersDwh) {

			if (!isExistTransferFromRobo(transfer, transfersRobo)) {
				saveList.add(transfer);
			}
		}
		boolean success = saveTransferToRobo(saveList);
		log.info("TransferSynchronized.synchorizedTransferData() done ...");

		return success;


	}

	private boolean saveTransferToRobo(List<Transfer> saveList) throws SecurityException, IOException {
		log.info("TransferSynchronized.saveTransferToRobo()"); 
		Connection conRobo = DbOperation.getRoboDbConnection();

		// the mysql insert statement
		String query = " insert into roboback.transfer "
				+ "(SIPARIS_TIPI, SIPARIS_NO, ALICI_TRANSFER_NO, SIPARIS_DURUM, SON_DEGISTIREN , OLUSTURMA_TARIHI,SATICI_NO,"
				+ "SATICI_ADI,SEVK_TARIHI,ART_NO,ART_DURUM,SIPARIS_ADET,SIPARIS_GIRILEN,STATUS)"
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {

			for (Transfer trans : saveList) {
				PreparedStatement preparedStmt = conRobo.prepareStatement(query);

				preparedStmt.setString(1, trans.getSIPARIS_TIPI());

				preparedStmt.setString(2, trans.getSIPARIS_NO());

				preparedStmt.setString(3, trans.getALICI_TRANSFER_NO());

				preparedStmt.setString(4, trans.getSIPARIS_DURUM());

				preparedStmt.setString(5, trans.getSON_DEGISTIREN());

				preparedStmt.setString(6, trans.getOLUSTURMA_TARIHI());

				preparedStmt.setString(7, trans.getSATICI_NO());

				preparedStmt.setString(8, trans.getSATICI_ADI());

				preparedStmt.setString(9, trans.getSEVK_TARIHI());

				preparedStmt.setString(10, trans.getART_NO());

				preparedStmt.setString(11, trans.getART_DURUM());

				preparedStmt.setString(12, trans.getSIPARIS_ADET());

				preparedStmt.setString(13, trans.getSIPARIS_GIRILEN());

				preparedStmt.setString(14, trans.getSTATUS());

				// execute the preparedstatement
				preparedStmt.execute();
			}
			conRobo.close();
			log.info("TransferSynchronized.saveTransferToRobo() is done" ); 
			return true;
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		}
		log.info("TransferSynchronized.saveTransferToRobo() is done" ); 
		return false;

	}

	private boolean isExistTransferFromRobo(Transfer transfer, List<Transfer> transfersRobo) throws SecurityException, IOException {
		for (Transfer transRobo : transfersRobo) {
			try {
				if (transRobo.getSIPARIS_TIPI().equals(transfer.getSIPARIS_TIPI()) &&

						isEquals(transRobo.getSIPARIS_NO(), transfer.getSIPARIS_NO()) &&

						isEquals(transRobo.getALICI_TRANSFER_NO(), transfer.getALICI_TRANSFER_NO()) &&

						isEquals(transRobo.getSIPARIS_DURUM(), transfer.getSIPARIS_DURUM()) &&

						isEquals(transRobo.getSON_DEGISTIREN(), transfer.getSON_DEGISTIREN()) &&

						isEquals(transRobo.getOLUSTURMA_TARIHI(), transfer.getOLUSTURMA_TARIHI()) &&

						isEquals(transRobo.getSATICI_NO(), transfer.getSATICI_NO()) &&

						isEquals(transRobo.getSATICI_ADI(), transfer.getSATICI_ADI()) &&

						isEquals(transRobo.getSEVK_TARIHI(), transfer.getSEVK_TARIHI()) &&

						isEquals(transRobo.getART_NO(), transfer.getART_NO()) &&

						isEquals(transRobo.getART_DURUM(), transfer.getART_DURUM()) &&

						isEquals(transRobo.getSIPARIS_ADET(), transfer.getSIPARIS_ADET()) &&

						isEquals(transRobo.getSIPARIS_GIRILEN(), transfer.getSIPARIS_GIRILEN())) {
					return true;
				}
			} catch (NullPointerException e) {
				log.debug(transfer.toString());
			}

		}
		log.info("TransferSynchronized.isExistTransferFromRobo() is done" ); 
		return false;
	}
	
	public boolean isEquals(String s1 ,String s2) {
		
		if (s1 == null && s2 == null) {
			return true;
		}else if (s1 == null && s2 != null) {
			return false;
		}else if(s1 != null && s2 == null) {
			return false;
		}
		return s1.equals(s2);
	}

	private List<Transfer> getTransferDataFromRobo() throws SecurityException, IOException {
		log.info("TransferSynchronized.getTransferDataFromRobo()");  
		String sql = "select * from roboback.transfer";

		Connection conDwh = DbOperation.getRoboDbConnection();

		ResultSet resultSet = null;

		try {
			resultSet = DbOperation.getQueryResultSet(conDwh, sql);

			List<Transfer> transferList = new ArrayList<Transfer>();

			while (resultSet.next()) {
				Transfer trans = new Transfer();

				trans.setSIPARIS_TIPI(resultSet.getString("SIPARIS_TIPI"));

				trans.setSIPARIS_NO(resultSet.getString("SIPARIS_NO"));

				trans.setALICI_TRANSFER_NO(resultSet.getString("ALICI_TRANSFER_NO"));

				trans.setSIPARIS_DURUM(resultSet.getString("SIPARIS_DURUM"));

				trans.setSON_DEGISTIREN(resultSet.getString("SON_DEGISTIREN"));

				trans.setOLUSTURMA_TARIHI(resultSet.getString("OLUSTURMA_TARIHI"));

				trans.setSATICI_NO(resultSet.getString("SATICI_NO"));

				trans.setSATICI_ADI(resultSet.getString("SATICI_ADI"));

				trans.setSEVK_TARIHI(resultSet.getString("SEVK_TARIHI"));

				trans.setART_NO(resultSet.getString("ART_NO"));

				trans.setART_DURUM(resultSet.getString("ART_DURUM"));

				trans.setSIPARIS_ADET(resultSet.getString("SIPARIS_ADET"));

				trans.setSIPARIS_GIRILEN(resultSet.getString("SIPARIS_GIRILEN"));

				transferList.add(trans);
			}
			log.info("TransferSynchronized.getTransferDataFromRobo() is done");  
			return transferList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
					} finally {
			// We have to close the connection and release the resources used.
			// Closing the statement results in closing the resultSet as well.
			try {
				resultSet.close();
			} catch (SQLException e) {
				log.debug(e.getMessage());
			}

			try {
				conDwh.close();
			} catch (SQLException e) {
				log.debug(e.getMessage());
			}
		}
		log.info("TransferSynchronized.getTransferDataFromRobo() is done");  
		return new ArrayList<Transfer>();

	}

	public List<Transfer> getTransferDataFromDwh() throws SecurityException, IOException {
		log.info("TransferSynchronized.getTransferDataFromDwh()");
		String sql = "SELECT \r\n" + 
				"CASE WHEN A.BESTELL_ART='TA' THEN 'OUTGOING TRANSFER'\r\n" + 
				"       WHEN A.BESTELL_ART='TB' THEN 'TRANSFER ORDER'\r\n" + 
				"       WHEN A.BESTELL_ART='R' THEN 'RETURN'\r\n" + 
				"       WHEN A.BESTELL_ART='B' THEN 'ORDER'\r\n" + 
				"       ELSE A.BESTELL_ART END AS SIPARIS_TIPI,\r\n" + 
				"A.BESTELL_NO SIPARIS_NO,\r\n" + 
				"A.BESTELL_NO_EXT AS ALICI_TRANSFER_NO,\r\n" + 
				"A.STATUS SIPARIS_DURUM,\r\n" + 
				"A.BEARB_NAME AS SON_DEGISTIREN,\r\n" + 
				"A.BESTELLT_AM OLUSTURMA_TARIHI,\r\n" + 
				"A.LIEF_NO SATICI_NO,\r\n" + 
				"A.LIEF_NAME SATICI_ADI,\r\n" + 
				"A.LIEFERDATUM SEVK_TARIHI,\r\n" + 
				"B.ART_NO,\r\n" + 
				"B.STATUS ART_DURUM,\r\n" + 
				"B.MENGE SIPARIS_ADET,\r\n" + 
				"B.MENGE_GEBUCHT SIPARIS_GIRILEN\r\n" + 
				"FROM W_BESTELL_PRIM A\r\n" + 
				"INNER JOIN W_BESTELL_ART B ON A.BESTELL_NO=b.BESTELL_NO";

		Connection conDwh = DbOperation.getDwhConnection();

		ResultSet resultSet = null;

		try {
			resultSet = DbOperation.getQueryResultSet(conDwh, sql);

			List<Transfer> transferList = new ArrayList<Transfer>();

			while (resultSet.next()) {
				Transfer trans = new Transfer();

				trans.setSIPARIS_TIPI(resultSet.getString("SIPARIS_TIPI"));

				trans.setSIPARIS_NO(resultSet.getString("SIPARIS_NO"));

				trans.setALICI_TRANSFER_NO(resultSet.getString("ALICI_TRANSFER_NO"));

				trans.setSIPARIS_DURUM(resultSet.getString("SIPARIS_DURUM"));

				trans.setSON_DEGISTIREN(resultSet.getString("SON_DEGISTIREN"));

				trans.setOLUSTURMA_TARIHI(resultSet.getString("OLUSTURMA_TARIHI"));

				trans.setSATICI_NO(resultSet.getString("SATICI_NO"));

				trans.setSATICI_ADI(resultSet.getString("SATICI_ADI"));

				trans.setSEVK_TARIHI(resultSet.getString("SEVK_TARIHI"));

				trans.setART_NO(resultSet.getString("ART_NO"));

				trans.setART_DURUM(resultSet.getString("ART_DURUM"));

				trans.setSIPARIS_ADET(resultSet.getString("SIPARIS_ADET"));

				trans.setSIPARIS_GIRILEN(resultSet.getString("SIPARIS_GIRILEN"));

				transferList.add(trans);
			}
			log.info("TransferSynchronized.getTransferDataFromDwh() is done");
			return transferList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
		} finally {
			// We have to close the connection and release the resources used.
			// Closing the statement results in closing the resultSet as well.
			try {
				resultSet.close();
			} catch (SQLException e) {
				log.debug(e.getMessage());
			}

			try {
				conDwh.close();
			} catch (SQLException e) {
				log.debug(e.getMessage());
			}
		}
		log.info("TransferSynchronized.getTransferDataFromDwh() is done");
		return new ArrayList<Transfer>();
	}

}
