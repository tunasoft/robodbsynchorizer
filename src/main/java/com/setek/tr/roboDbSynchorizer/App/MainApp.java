package com.setek.tr.roboDbSynchorizer.App;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.setek.tr.roboDbSynchorizer.Synchorizer.Worker;




public class MainApp {

	private static final Logger log = Logger.getLogger(MainApp.class);


	public static void main(String[] args) throws SQLException, IOException {

		log.info("MainApp.main()");

		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(new Worker(), 0, 10, TimeUnit.MINUTES);

		log.info("MainApp.main() is done");
	}

}
