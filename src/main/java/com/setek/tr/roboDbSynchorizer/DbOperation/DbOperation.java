package com.setek.tr.roboDbSynchorizer.DbOperation;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;



public class DbOperation {
	private static final Logger log=Logger.getLogger(DbOperation.class);
	

	
	
	public static Connection getRoboDbConnection() {
		log.info("DbOperation.getRoboDbConnection()");
		String url = "jdbc:mysql://localhost:3306/roboback";
		String username = "root";
		String password = "Mdepo1";

		// Load the MySQL driver.
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conRobo = DriverManager.getConnection(url, username, password);
			log.info("DbOperation.getRoboDbConnection() is done");
			return conRobo;
			
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage());
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		log.info("DbOperation.getRoboDbConnection() is done");
		return null;
	}

	public static Connection getDwhConnection() {
		log.info("DbOperation.getDwhConnection()");
		String url = "jdbc:odbc:MMWHKOCAELI1";
		String username = "TR_REPORTING";
		String password = "ADfm4Sd23";

		
		Properties props = new Properties();
		props.put("user", username);
		props.put("password", password);
		props.put("General.InternalCharEncoding","UTF-8");
		props.put("ODBCCharBinding","locale:tr_tr.1252");
		props.put("charSet", "ISO-8859-1");
		props.put("useUnicode" ,"true");
		props.put("Client.ODBCCharBinding","locale:tr_tr.1252");

		
//		String url = "jdbc:mysql://localhost:3306/DWH?useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=Turkey";
//		String username = "root";
//		String password = "Mdepo1";

		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			Connection conDwh = DriverManager.getConnection(url,props);
			log.info("DbOperation.getDwhConnection() is done");
			return conDwh;
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage());
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		log.info("DbOperation.getDwhConnection() is done");
		return null;

	}
	
	public static ResultSet getQueryResultSet(Connection con, String sqlQuery) {
		
		log.info("DbOperation.getQueryResultSet() sqlQuery -> " + sqlQuery );
		Statement statement;
		try {
			statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(sqlQuery);
			log.info("DbOperation.getQueryResultSet() is done");
			return resultSet;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		log.info("DbOperation.getQueryResultSet() is done");
		return null;
	
	}
	

}
