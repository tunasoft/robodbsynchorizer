package com.setek.tr.roboDbSynchorizer.model;

public class Lief {

	private String SATICI_NO;
	private String SATICI_ADI;
	private String TAX_NUMBER;
	private String ADRES;
	private String SEHIR;
	private String STATUS = "0";
	private String TAX_OFFICE;
	private String ULKE_KODU;
	private String VC_FLAG;
	private String POSTA_KODU;
	private String TELEFON;
	private String EMAIL;
	
	public String getSATICI_NO() {
		return SATICI_NO;
	}

	public void setSATICI_NO(String sATICI_NO) {
		SATICI_NO = sATICI_NO;
	}

	public String getSATICI_ADI() {
		return SATICI_ADI;
	}

	public void setSATICI_ADI(String sATICI_ADI) {
		SATICI_ADI = sATICI_ADI;
	}

	

	public String getTAX_NUMBER() {
		return TAX_NUMBER;
	}

	public void setTAX_NUMBER(String tAX_NUMBER) {
		TAX_NUMBER = tAX_NUMBER;
	}

	public String getADRES() {
		return ADRES;
	}

	public void setADRES(String aDRES) {
		ADRES = aDRES;
	}

	public String getSEHIR() {
		return SEHIR;
	}

	public void setSEHIR(String sEHIR) {
		SEHIR = sEHIR;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getTAX_OFFICE() {
		return TAX_OFFICE;
	}

	public void setTAX_OFFICE(String tAX_OFFICE) {
		TAX_OFFICE = tAX_OFFICE;
	}

	public String getULKE_KODU() {
		return ULKE_KODU;
	}

	public void setULKE_KODU(String uLKE_KODU) {
		ULKE_KODU = uLKE_KODU;
	}

	public String getVC_FLAG() {
		return VC_FLAG;
	}

	public void setVC_FLAG(String vC_FLAG) {
		VC_FLAG = vC_FLAG;
	}

	public String getPOSTA_KODU() {
		return POSTA_KODU;
	}

	public void setPOSTA_KODU(String pOSTA_KODU) {
		POSTA_KODU = pOSTA_KODU;
	}

	public String getTELEFON() {
		return TELEFON;
	}

	public void setTELEFON(String tELEFON) {
		TELEFON = tELEFON;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

}
