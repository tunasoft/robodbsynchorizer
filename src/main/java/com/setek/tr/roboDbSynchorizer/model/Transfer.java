package com.setek.tr.roboDbSynchorizer.model;

public class Transfer {

	private String SIPARIS_TIPI;
	private String SIPARIS_NO;
	private String ALICI_TRANSFER_NO;
	private String SIPARIS_DURUM;
	private String SON_DEGISTIREN;
	private String OLUSTURMA_TARIHI;
	private String SATICI_NO;
	private String SATICI_ADI;
	private String SEVK_TARIHI;
	private String ART_NO;
	private String ART_DURUM;
	private String SIPARIS_ADET;
	private String SIPARIS_GIRILEN;
	private String STATUS = "0";

	public String getSIPARIS_TIPI() {
		return SIPARIS_TIPI;
	}

	public void setSIPARIS_TIPI(String sIPARIS_TIPI) {
		SIPARIS_TIPI = sIPARIS_TIPI;
	}

	public String getSIPARIS_NO() {
		return SIPARIS_NO;
	}

	public void setSIPARIS_NO(String sIPARIS_NO) {
		SIPARIS_NO = sIPARIS_NO;
	}

	public String getALICI_TRANSFER_NO() {
		return ALICI_TRANSFER_NO;
	}

	public void setALICI_TRANSFER_NO(String aLICI_TRANSFER_NO) {
		ALICI_TRANSFER_NO = aLICI_TRANSFER_NO;
	}

	public String getSIPARIS_DURUM() {
		return SIPARIS_DURUM;
	}

	public void setSIPARIS_DURUM(String sIPARIS_DURUM) {
		SIPARIS_DURUM = sIPARIS_DURUM;
	}

	public String getSON_DEGISTIREN() {
		return SON_DEGISTIREN;
	}

	public void setSON_DEGISTIREN(String sON_DEGISTIREN) {
		SON_DEGISTIREN = sON_DEGISTIREN;
	}

	public String getOLUSTURMA_TARIHI() {
		return OLUSTURMA_TARIHI;
	}

	public void setOLUSTURMA_TARIHI(String oLUSTURMA_TARIHI) {
		OLUSTURMA_TARIHI = oLUSTURMA_TARIHI;
	}

	public String getSATICI_NO() {
		return SATICI_NO;
	}

	public void setSATICI_NO(String sATICI_NO) {
		SATICI_NO = sATICI_NO;
	}

	public String getSATICI_ADI() {
		return SATICI_ADI;
	}

	public void setSATICI_ADI(String sATICI_ADI) {
		SATICI_ADI = sATICI_ADI;
	}

	public String getSEVK_TARIHI() {
		return SEVK_TARIHI;
	}

	public void setSEVK_TARIHI(String sEVK_TARIHI) {
		SEVK_TARIHI = sEVK_TARIHI;
	}

	public String getART_NO() {
		return ART_NO;
	}

	public void setART_NO(String aRT_NO) {
		ART_NO = aRT_NO;
	}

	public String getART_DURUM() {
		return ART_DURUM;
	}

	public void setART_DURUM(String aRT_DURUM) {
		ART_DURUM = aRT_DURUM;
	}

	public String getSIPARIS_ADET() {
		return SIPARIS_ADET;
	}

	public void setSIPARIS_ADET(String sIPARIS_ADET) {
		SIPARIS_ADET = sIPARIS_ADET;
	}

	public String getSIPARIS_GIRILEN() {
		return SIPARIS_GIRILEN;
	}

	public void setSIPARIS_GIRILEN(String sIPARIS_GIRILEN) {
		SIPARIS_GIRILEN = sIPARIS_GIRILEN;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

}
