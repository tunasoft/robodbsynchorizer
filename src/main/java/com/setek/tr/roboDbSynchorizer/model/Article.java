package com.setek.tr.roboDbSynchorizer.model;

public class Article {

	private String ART_NO;
	private String ART_NAME;
	private String ART_TYP;
	private String LOESCH_KZ;
	private String BRAND;
	private String EAN_NO;
	private String STATUS = "0";
	private String UOM_PURCHASING;
	private String COLOR_CODE;
	private String LOCAL_PRODUCT_CODE;
	private String ARTICLE_CATEGORY;

	public String getART_NO() {
		return ART_NO;
	}

	public void setART_NO(String aRT_NO) {
		ART_NO = aRT_NO;
	}

	public String getART_NAME() {
		return ART_NAME;
	}

	public void setART_NAME(String aRT_NAME) {
		ART_NAME = aRT_NAME;
	}

	public String getART_TYP() {
		return ART_TYP;
	}

	public void setART_TYP(String aRT_TYP) {
		ART_TYP = aRT_TYP;
	}

	public String getLOESCH_KZ() {
		return LOESCH_KZ;
	}

	public void setLOESCH_KZ(String lOESCH_KZ) {
		LOESCH_KZ = lOESCH_KZ;
	}

	public String getBRAND() {
		return BRAND;
	}

	public void setBRAND(String bRAND) {
		BRAND = bRAND;
	}

	public String getEAN_NO() {
		return EAN_NO;
	}

	public void setEAN_NO(String eAN_NO) {
		EAN_NO = eAN_NO;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getUOM_PURCHASING() {
		return UOM_PURCHASING;
	}

	public void setUOM_PURCHASING(String uOM_PURCHASING) {
		UOM_PURCHASING = uOM_PURCHASING;
	}

	public String getCOLOR_CODE() {
		return COLOR_CODE;
	}

	public void setCOLOR_CODE(String cOLOR_CODE) {
		COLOR_CODE = cOLOR_CODE;
	}

	public String getLOCAL_PRODUCT_CODE() {
		return LOCAL_PRODUCT_CODE;
	}

	public void setLOCAL_PRODUCT_CODE(String lOCAL_PRODUCT_CODE) {
		LOCAL_PRODUCT_CODE = lOCAL_PRODUCT_CODE;
	}

	public String getARTICLE_CATEGORY() {
		return ARTICLE_CATEGORY;
	}

	public void setARTICLE_CATEGORY(String aRTICLE_CATEGORY) {
		ARTICLE_CATEGORY = aRTICLE_CATEGORY;
	}

}
